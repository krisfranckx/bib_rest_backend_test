package be.atheneumboom.bibliotheek.model;

import jakarta.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "books")
public class Book extends BibItem implements Comparable<Book> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long boekId;
    private String auteur;
    @Enumerated
    private Taal taal;
    private String graad;
    private String fictieNonFictie;

    public Book() {
        super();
    }

    public Book(String titel, String auteur, Taal taal, String graad, String fictieNonFictie) {
        super(titel);
        System.out.println("Boek aangemaakt");
        this.auteur = auteur;
        this.taal = taal;
        this.graad = graad;
        this.fictieNonFictie = fictieNonFictie;
        super.setType("book");
    }

    @Override
    public int compareTo(Book o) {
        return this.auteur.compareToIgnoreCase(o.getAuteur());
    }

    @Override
    public String toString() {
        return "Book{" +
                ", auteur='" + auteur + '\'' +
                ", taal=" + taal +
                ", graad='" + graad + '\'' +
                ", fictieNonFictie='" + fictieNonFictie + '\'' +
                super.toString()+"} ";
    }
}
