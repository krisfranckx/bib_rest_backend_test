package be.atheneumboom.bibliotheek.model;

import jakarta.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "magazines")
public class Magazine extends BibItem implements Comparable<Magazine> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long magazineId;
    private int nummertijdschrift;
    private int jaargang;

    public Magazine(String titel, int nummertijdschrift, int jaargang) {
        super(titel);
        System.out.println("Magazine aangemaakt");
        this.nummertijdschrift = nummertijdschrift;
        this.jaargang = jaargang;
        super.setType("magazine");
    }

    public Magazine() {
        super();
    }

    public int compareTo(Magazine magazine) {
        return this.getTitel().compareToIgnoreCase(magazine.getTitel());
    }
}
