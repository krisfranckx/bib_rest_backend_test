package be.atheneumboom.bibliotheek.config;

import be.atheneumboom.bibliotheek.model.User;

public class Settings {
    public static final String BASE_URL="http://localhost:5000";
    public static final int PAGESIZE_USERS = 10;
    public static final String PAGESORT_USERS = "voornaam";
    public static final int PAGESIZE_BOOKS = 10;
    public static final String PAGESORT_BOOKS = "auteur";
}
