package be.atheneumboom.bibliotheek.repository;

import be.atheneumboom.bibliotheek.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookRepo extends JpaRepository<Book,Long> {

    @Query(value = "SELECT * FROM books b WHERE " +
            "lower(b.auteur) LIKE CONCAT('%',:keywordFilter,'%') OR " +
            "lower(b.titel) LIKE CONCAT('%',:keywordFilter,'%') OR " +
            "lower(b.code) LIKE CONCAT('%',:keywordFilter,'%') OR " +
            "lower(b.taal) LIKE CONCAT('%',:keywordFilter,'%')"         //taal werkt niet????
            , nativeQuery = true)
    Page<Book> findAll(PageRequest withSort, String keywordFilter);
}
