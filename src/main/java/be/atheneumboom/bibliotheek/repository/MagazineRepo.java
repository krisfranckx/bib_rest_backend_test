package be.atheneumboom.bibliotheek.repository;

import be.atheneumboom.bibliotheek.model.Magazine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MagazineRepo extends JpaRepository<Magazine,Long> {
}
