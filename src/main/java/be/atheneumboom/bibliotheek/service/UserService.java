package be.atheneumboom.bibliotheek.service;

import be.atheneumboom.bibliotheek.model.DTO.UserDTO;
import be.atheneumboom.bibliotheek.model.User;
import org.springframework.data.domain.Page;

public interface UserService {
    Page<UserDTO> getUsers(Integer offset, Integer pagesize, String sortField, String keywordFilter);

    UserDTO getUser(Long userId);
    UserDTO saveUser(User user);
    boolean deleteUser(Long userId);

    UserDTO updateUser(Long id, User user);
}
