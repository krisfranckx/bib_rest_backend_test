package be.atheneumboom.bibliotheek.service.impl;

import be.atheneumboom.bibliotheek.model.BibItem;
import be.atheneumboom.bibliotheek.model.Book;
import be.atheneumboom.bibliotheek.model.Magazine;
import be.atheneumboom.bibliotheek.repository.BookRepo;
import be.atheneumboom.bibliotheek.repository.MagazineRepo;
import be.atheneumboom.bibliotheek.service.BibItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
@RequiredArgsConstructor
public class BibItemServiceImpl implements BibItemService {
    private final BookRepo bookRepo;
    private final MagazineRepo magazineRepo;
    @Override
    public Collection<BibItem> getBibItems() {
        Collection<BibItem> collection = new ArrayList<>();
        collection.addAll(bookRepo.findAll());
        collection.addAll(magazineRepo.findAll());
        return collection;
    }


    @Override
    public BibItem saveBibItem(BibItem bibItem) {
        if (bibItem.getClass().getName().toLowerCase().contains("book")){
            bookRepo.save((Book) bibItem);
        }else {
            magazineRepo.save((Magazine) bibItem);
        }
        return bibItem;
    }
}
