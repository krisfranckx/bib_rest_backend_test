package be.atheneumboom.bibliotheek.service.impl;

import be.atheneumboom.bibliotheek.config.Settings;
import be.atheneumboom.bibliotheek.exceptions.BookNotFoundException;
import be.atheneumboom.bibliotheek.model.Book;
import be.atheneumboom.bibliotheek.repository.BookRepo;
import be.atheneumboom.bibliotheek.service.BibItemService;
import be.atheneumboom.bibliotheek.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepo bookRepo;
    private final BibItemService bibItemService;

    @Override
    public Page<Book> getBooks(Integer pageNumber, Integer pageSize, String sortField, String keywordFilter) {
        Page<Book> books;
        if (keywordFilter==null){
            books = bookRepo.findAll(
                    PageRequest.of(
                            pageNumber!=null?pageNumber:0, pageSize!=null?pageSize:Settings.PAGESIZE_BOOKS).
                            withSort(Sort.by(sortField!=null?sortField:Settings.PAGESORT_BOOKS)) );
        }else{
            System.out.println(keywordFilter.trim().toLowerCase());
            books = bookRepo.findAll(
                    PageRequest.of(
                            pageNumber!=null?pageNumber:0, pageSize!=null?pageSize:Settings.PAGESIZE_BOOKS).
                            withSort(Sort.by(sortField!=null?sortField:Settings.PAGESORT_BOOKS)), keywordFilter.trim().toLowerCase() );
        }

        List<Book> booksList = books.stream().toList();
        return new PageImpl<>(booksList);
    }

    @Override
    public Book saveBook(Book book) {
        return (Book)bibItemService.saveBibItem(
                new Book(book.getTitel(), book.getAuteur(), book.getTaal(), book.getGraad(), book.getFictieNonFictie())
        );
    }

    @Override
    public Book updateBook(Long id, Book book) {
        Book bookUpdated = bookRepo.findById(id).orElseThrow(()-> new BookNotFoundException("Boek met id "+id+ " niet gevonden."));
        bookUpdated.setAuteur   (book.getAuteur());
        bookUpdated.setTaal     (book.getTaal());
        bookUpdated.setGraad    (book.getGraad());
        bookUpdated.setTitel    (book.getTitel());
        bookUpdated.setFictieNonFictie  (book.getFictieNonFictie());
        return bookRepo.save(
                bookUpdated
        );
    }

    @Override
    public void deleteBook(Long id) {
        bookRepo.deleteById(id);
    }

    @Override
    public Book getBook(Long id) {
        return bookRepo.findById(id).orElseThrow(()->new BookNotFoundException("Boek met id "+id + " niet gevonden."));
    }
}
