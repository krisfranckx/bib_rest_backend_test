package be.atheneumboom.bibliotheek.service;

import be.atheneumboom.bibliotheek.model.BibItem;
import be.atheneumboom.bibliotheek.model.Book;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface BookService {
    Page<Book> getBooks(Integer offset, Integer pagesize, String sortField, String keywordFilter);

    Book saveBook(Book book);

    Book updateBook(Long id, Book book);
    void deleteBook(Long id);

    Book getBook(Long id);
}
