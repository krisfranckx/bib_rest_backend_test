
package be.atheneumboom.bibliotheek.controller;

import be.atheneumboom.bibliotheek.model.Book;
import be.atheneumboom.bibliotheek.model.Response;
import be.atheneumboom.bibliotheek.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.Map;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/admin/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping("")
    public ResponseEntity<Response> getBooks(@RequestParam(value = "pageSize", required = false) Integer pageSize,
                                             @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                             @RequestParam(value = "filter", required = false) String filter,
                                             @RequestParam(value = "sort", required = false) String sort){
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("books",bookService.getBooks(pageNumber, pageSize, sort, filter).stream().toList()))
                        .message("Alle boeken opgehaald")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }
    @GetMapping("/{id}")
    public ResponseEntity<Response> getBook(@PathVariable("id")Long id){
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("book",bookService.getBook(id)))
                        .message("Boek met id "+id+" opgehaald")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }
    @PostMapping ()
    public ResponseEntity<Response> saveBook(@RequestBody Book book){
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("book",bookService.saveBook(book)))
                        .message("Boek bewaard: "+book)
                        .status(CREATED)
                        .statusCode(CREATED.value())
                        .build());
    }

    @PutMapping ("{bookId}")
    public ResponseEntity<Response> updateBook(@RequestBody Book book, @PathVariable("bookId") Long bookId){
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("book",bookService.updateBook(bookId, book)))
                        .message("Boek aangepast: "+book)
                        .status(OK)
                        .statusCode(OK.value())
                        .build());
    }

    @DeleteMapping("{bookId}")
    public ResponseEntity<Response> deleteBoek(@PathVariable("bookId") Long bookId) {
        bookService.deleteBook(bookId);
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .message("Boek gedeleted met id " + bookId)
                        .status(OK)
                        .statusCode(OK.value())
                        .build());
    }
}

