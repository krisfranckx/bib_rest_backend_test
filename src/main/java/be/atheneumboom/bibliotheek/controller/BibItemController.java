
package be.atheneumboom.bibliotheek.controller;

import be.atheneumboom.bibliotheek.model.Response;
import be.atheneumboom.bibliotheek.service.BibItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/admin/items")
@RequiredArgsConstructor
public class BibItemController {
    private final BibItemService bibItemService;

    @GetMapping("")
    public ResponseEntity<Response> getBibItems(){
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("bibItems",bibItemService.getBibItems().stream().toList()))
                        .message("Alle bibitems opgehaald")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }
/*
@DeleteMapping
    public ResponseEntity<Response> deleteBibItem(@RequestBody @Valid BibItem bibItem) throws InvocationTargetException, IllegalAccessException {  //@Valid kijkt na of de validatie ok is (zie klasse Boek @NotEmpty)
        return ResponseEntity.ok(  //eigenlijk moet dit created zijn. Wat met URI-location?
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("bibItemDeleted",bibItemService.deleteBibItem(bibItem.getItemId())))
                        .message("Bibitem verwijderd met id "+bibItem.getItemId())
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }


@GetMapping("/{id}")  //test Postman OK
    public ResponseEntity<Response> getBibItem(@PathVariable("id") Long id){
        return ResponseEntity.ok(
                Response.builder()//zie builderpattern annotatie in Response klasse
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("bibItem",bibItemService.getBibItem(id)))
                        .message("Item opgehaald met id "+id)
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }*/



}

