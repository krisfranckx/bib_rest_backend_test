package be.atheneumboom.bibliotheek;

import be.atheneumboom.bibliotheek.model.Book;
import be.atheneumboom.bibliotheek.model.Magazine;
import be.atheneumboom.bibliotheek.model.Taal;
import be.atheneumboom.bibliotheek.model.User;
import be.atheneumboom.bibliotheek.service.BibItemService;
import be.atheneumboom.bibliotheek.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


import java.util.Arrays;


@SpringBootApplication
public class BibliotheekApplication {

	public static void main(String[] args){
		SpringApplication.run(BibliotheekApplication.class, args);
	}

	@Bean
	CommandLineRunner run(BibItemService bibItemService, UserService userService) {

		return args -> {
			bibItemService.saveBibItem(new Book("Lalala", "Eva Francx", Taal.Spaans, "3de graad", "Non-fictie"));
			bibItemService.saveBibItem(new Book("Titel van boek 1", "Kris Franckx", Taal.Engels, "2de graad", "Fictie"));
			bibItemService.saveBibItem(new Book("TestTitel van boek 2", "Eva Chaubet", Taal.Frans, "3de graad", "Non-fictie"));
			bibItemService.saveBibItem(new Book("Blabla", "Eva Chaubet", Taal.Spaans, "3de graad", "Non-fictie"));
			bibItemService.saveBibItem(new Magazine("Titel van magzine 1", 44, 1987));
			bibItemService.saveBibItem(new Magazine("TestTitel van magzine 2", 55, 2001));
			userService.saveUser(new User("Kris", "Franckx", "kris@email.com"));
			userService.saveUser(new User("Eva", "Chaubet", "eva@email.com"));
			userService.saveUser(new User("Polly", "Franckx", "polly@email.com"));
			userService.saveUser(new User("Louis", "Franckx", "louis@email.com"));
			userService.saveUser(new User("Pablo", "Chaubet", "pablo@email.com"));
		};
	}

	/*@Bean
	public CorsFilter corsFilter(){
		UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:3000","http://localhost:4200"));
		corsConfiguration.setAllowedMethods(Arrays.asList("GET","POST", "DELETE", "PUT"));
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**",corsConfiguration);
		return new org.springframework.web.filter.CorsFilter(urlBasedCorsConfigurationSource);

	}*/

}



